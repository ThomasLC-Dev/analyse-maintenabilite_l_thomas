package fr.epsi.enums;

import fr.epsi.interfaces.PriceCalculator;

public enum CarType {
    REGULAR(daysRented -> {
        double result = 5000 + daysRented * 9500;
        if (daysRented > 5) {
            result -= (daysRented - 2) * 10000;
        }
        return result/100;
    }),
    NEW_MODEL(daysRented -> {
        double result = 9000 + daysRented * 15000;
        if (daysRented > 3) {
            result -= (daysRented - 2) * 10000;
        }
        return result/100;
    });

    private PriceCalculator priceCalculator;

    CarType(PriceCalculator priceCalculator) {
        this.priceCalculator = priceCalculator;
    }

    public double getPrice(int daysRented) {
        return priceCalculator.calculate(daysRented);
    }
}
