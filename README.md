
# Analyse / Maintenabilité

- [Analyse / Maintenabilité](#analyse---maintenabilit-)
  * [Exercice 1](#exercice-1)
  * [Exercice 2](#exercice-2)
    * [Installation](#installation)
    * [Tests](#tests)

## Exercice 1

 1. Les principales sources de complexité dans un système logiciel sont :
    - La nature du problème résolu par le système
    - Le domaine d'application
    - La complexité algorithmique
    - La complexité de la structure du code
    - La complexité des exigences
    - La complexité des environnements
    - La complexité techniques
2. Les avantages sont :
    - La modularité : Remplacement simplifié sans affecter les autres parties du système
    - La flexibilité et l'extensibilité : Faciliter l'ajout de nouvelles fonctionnalités
    - Réutilisabilité : Réutilisation simplifiée dû aux définitions présentes sur les interfaces
    - Testabilité : Tester spécifiquement les comportements de l'interface plutôt que l'implémentation
    - Maintenabilité : Facilité de compréhension et de maintenabilité dû à l'abstraction
3. Compréhension des étapes de l'heuristique d'Eric Steven Raymond
    - "First make it run" : Rendre le système opérationnel, même de manière basique -> Obtenir une version fonctionnelle du système
    - "Next make it correct" : S'assurer que le système se comporte correctement -> Corriger les bugs, valider les exigences et validation des résultats produits
    - "Only after that worry about making it fast" : Se concentrer sur l'optimisation des performances -> Amélioration de l'efficacité et de la vitesse du système sans compromettre son comportement ou sa fiabilité

    - Compréhension globale : Cette approche met l'accent sur l'importance de progresser de manière incrémentale et pragmatique. Elle permet de réduire les risques en divisant le développement en étapes gérables et en évitant les optimisations prématurées.
4. La méthode recommandée pour mener à bien un travail de refactoring est :
    - Comprendre le code existant
    - Ecrire les tests unitaires
    - Refactoriser par étapes incrémentales
    - Suivre les principes SOLID
    - Améliorer la lisibilité du code
    - Documenter les changements
## Exercice 2
## Installation

Lancement du projet sur un environnement possédant Java et Maven

```bash
  mvn package
  (Windows) java -jar target\refactoring-1.0-SNAPSHOT.jar
  (Linux/Mac) java -jar target/refactoring-1.0-SNAPSHOT.jar
```
## Tests

Lancement des tests du projet (3 tests présents)

```bash
  mvn test
```