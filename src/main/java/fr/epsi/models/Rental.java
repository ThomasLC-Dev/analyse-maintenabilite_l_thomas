package fr.epsi.models;

public class Rental {
    private Car car;
    private int daysRented;

    public Rental(Car car, int daysRented) {
        this.car = car;
        this.daysRented = daysRented;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public Car getCar() {
        return car;
    }

    public double getPrice() {
        return car.getPrice(this.getDaysRented());
    }
}
