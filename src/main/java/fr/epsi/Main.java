package fr.epsi;

import fr.epsi.enums.CarType;
import fr.epsi.models.Car;
import fr.epsi.models.Customer;
import fr.epsi.models.Invoice;
import fr.epsi.models.Rental;

public class Main {

    public static void main(String[] args){
        Car car1 = new Car("Car 1", CarType.REGULAR);
        Car car2 = new Car("Car 2", CarType.NEW_MODEL);

        Rental rental1 = new Rental(car1, 3);
        Rental rental2 = new Rental(car2, 2);

        Customer customer = new Customer("John Doe");
        customer.addRental(rental1);
        customer.addRental(rental2);

        Invoice invoice = new Invoice(customer);

        System.out.println(invoice);
        System.out.println(invoice.toJson());
    }

}
