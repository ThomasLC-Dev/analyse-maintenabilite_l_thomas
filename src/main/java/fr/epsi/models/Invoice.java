package fr.epsi.models;

import fr.epsi.enums.CarType;
import org.json.JSONArray;
import org.json.JSONObject;

public class Invoice {
    private Customer customer;
    private double price;
    private int frequentRenterPoints;

    public Invoice(Customer customer) {
        this.customer = customer;
        this.price = 0.0;
        this.frequentRenterPoints = 0;

        this.calculate();
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getFrequentRenterPoints() {
        return frequentRenterPoints;
    }

    public void setFrequentRenterPoints(int frequentRenterPoints) {
        this.frequentRenterPoints = frequentRenterPoints;
    }

    private void calculate() {
        for (Rental rental : this.customer.getRentals()) {
            double thisAmount = rental.getPrice();
            this.price += thisAmount;

            this.frequentRenterPoints++;


            if (rental.getCar().getCarType() == CarType.NEW_MODEL && rental.getDaysRented() > 1) {
                this.frequentRenterPoints++;
            }

        }

        this.frequentRenterPoints += (int) Math.round(this.price / 10);
    }

    public String toString() {
        StringBuilder result = new StringBuilder("Rental Record for " + this.customer.getName() + "\n");

        for (Rental rental : this.customer.getRentals()) {
            result.append("\t").append(rental.getCar().getTitle()).append("\t").append(String.format("%.1f", rental.getPrice())).append("\n");
        }

        // Add footer lines
        result.append("Amount owed is ").append(String.format("%.1f", this.price)).append("\n");
        result.append("You earned ").append(this.frequentRenterPoints).append(" frequent renter points\n");

        return result.toString();
    }

    public String toJson() {
        JSONObject result = new JSONObject();
        result.put("customerName", this.customer.getName());

        JSONArray rentals = new JSONArray();

        for (Rental rental : this.customer.getRentals()) {
            JSONObject rentalJson = new JSONObject();
            rentalJson.put("title", rental.getCar().getTitle());
            rentalJson.put("price", rental.getPrice());

            rentals.put(rentalJson);
        }

        result.put("rentals", rentals);
        result.put("price", this.price);
        result.put("frequentRenterPoints", this.frequentRenterPoints);

        return result.toString();
    }
}
