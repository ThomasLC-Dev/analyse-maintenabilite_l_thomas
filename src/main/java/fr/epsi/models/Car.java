package fr.epsi.models;

import fr.epsi.enums.CarType;

public class Car {
    private String title;
    private CarType type;

    public Car(String title, CarType type) {
        this.title = title;
        this.type = type;
    }

    public CarType getCarType() {
        return type;
    }

    public void setCarType(CarType type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice(int daysRented) {
        return type.getPrice(daysRented);
    }
}
