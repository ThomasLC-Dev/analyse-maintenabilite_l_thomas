package fr.epsi;

import fr.epsi.enums.CarType;
import fr.epsi.models.Car;
import fr.epsi.models.Customer;
import fr.epsi.models.Invoice;
import fr.epsi.models.Rental;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MainTest {

    private static Invoice invoice;

    @BeforeAll
    static void setUp() {
        Car car1 = new Car("Car 1", CarType.REGULAR);
        Car car2 = new Car("Car 2", CarType.NEW_MODEL);

        Rental rental1 = new Rental(car1, 3);
        Rental rental2 = new Rental(car2, 2);

        Customer customer = new Customer("John Doe");
        customer.addRental(rental1);
        customer.addRental(rental2);

        invoice = new Invoice(customer);
    }

    @Test
    void calculateRentalPrice(){
        Rental rental1 = invoice.getCustomer().getRentals().get(0);
        Rental rental2 = invoice.getCustomer().getRentals().get(1);
        double priceRental1 = rental1.getPrice();
        double priceRental2 = rental2.getPrice();

        assertEquals(335.0, priceRental1);
        assertEquals(390.0, priceRental2);
    }

    @Test
    void calculateInvoice(){
        assertEquals(725.0, invoice.getPrice());
        assertEquals(76, invoice.getFrequentRenterPoints());
    }

    @Test
    void exportToJson(){
        assertEquals("{\"price\":725,\"rentals\":[{\"price\":335,\"title\":\"Car 1\"},{\"price\":390,\"title\":\"Car 2\"}],\"frequentRenterPoints\":76,\"customerName\":\"John Doe\"}", invoice.toJson());
    }
}
