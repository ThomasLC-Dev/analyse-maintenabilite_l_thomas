package fr.epsi.interfaces;

public interface PriceCalculator {
    double calculate(int daysRented);
}
